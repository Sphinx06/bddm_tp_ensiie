# PROJET BDDM 

Le projet a été réalisé en python 3.

Les fichiers de code source sont présent dans le dossier SRC/.
Il est nécessaire d'avoir la librairie OPENCV pour profiter pleinement des fonctionnalités du programme.

Voici la commande pour installer OPENCV sur python3 :
```
pip3 install opencv-python
```

Pour lancer le projet: 
```
python3 SRC/TP_BDDM.py 
```

Le projet propose plusieurs solutions
1. Création des indexs en niveau de gris des images au format JSON.
2.  Création des indexs RGB des images au format JSON.
3. Création des fichiers HTML de correspondances d'images:
    Pour cette étape, il faut avoir au préalable effectuées les actions 1 et 2 pour créer les descripteurs en fichier au format JSON
    Sauf si celle-ci est déjà existante dans le dossier:
    BASE10000/
    
4. Affichage d'un exemple de diagrammes Précision Rappel (plot)
5. Affichage des diagrammes Précision Rappel pour une classe au choix
6. Affichage des diagrammes Précision Rappel globaux (toutes classes).
    Pour les étapes 4, 5 ,6, il faut tout d'abord avoir créer les fichier JSON de couple PR.
    Sauf si celle-ci sont déjà existante dans le dossier:
    BASE10000_PR/
    
 
Les modes suivants sont désactivés dans le code, à décommenter pour tester.
7. Création des fichiers PR JSON pour le mode GREY pour toutes les classes.
8. Création des fichiers PR JSON pour le mode RGB pour toutes les classes.

# Architecture du projet: 
```
Base10000/
		---- images/
		---- VT_HTML/
		---- VT_description.txt
		---- VT_files.txt
Base10000/
		---- Base10000_files.txt
		---- Description.txt
		---- HistGREY_#.json
		---- HistRGB_#x#x#.json
Base10000_HTML/
		---- All HTML FILES GENERATED
		---- index.html
Base10000_PR/
		---- PR_#CLASSE#_GREY.json
		---- PR_#CLASSE#_RGB.json	
SRC/
		---- const.py
		---- descriptors_index.py
		---- PR.py
		---- similarite.py
		---- TP_BDDM.py
		---- ALL_README
```
