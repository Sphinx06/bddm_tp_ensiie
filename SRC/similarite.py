#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# import the necessary packages
import numpy as np
import json 
import os.path
import operator

import const

#---------------------------------------------------------
# SIMILARITE 
#---------------------------------------------------------   
def findHistByFilename(jsonData,imageName):
    for dico in jsonData["index"]:
        if dico["filename"]==imageName:
            return dico["hist"]
    return None
    
#---------------------------------------------------------
# Calcul des distance 
# jsonFile : fichier d'index des images JSON  
# imageName: Nom de l'image à comparers
# k : Nombre d'images à retourner
#---------------------------------------------------------   
def QBE(jsonFile,imageName,k):
    # Vérification de l'existance de l'image
    if(os.path.exists(jsonFile)):
        distances = {}
        # Ouverture du fichier contenant la bibliothèque d'image
        with open(jsonFile) as json_data:
            data_dict = json.load(json_data)
            histOriginal = findHistByFilename(data_dict,imageName)
            for dico in data_dict["index"]:
                img = dico["filename"]
                dist = np.linalg.norm(np.array(histOriginal)-np.array(dico["hist"]))
                distances[img] = dist
            sorted_distances = sorted(distances.items(), key=operator.itemgetter(1))        
            if(k==-1):
                return sorted_distances
            else:
                return sorted_distances[:k+1]
    else:
        return {}
        print("Erreur le fichier n'existe pas.")
        
def createHTMLFromQBE(image,htmlName,distances,imagePath):
    # Création d'un dossier HMTL 
    if not os.path.exists(const.BASE10000_HTML_FOLDER):
        os.makedirs(const.BASE10000_HTML_FOLDER)
    
    # Création du fichier HTML 
    with open(const.BASE10000_HTML_FOLDER+htmlName, 'w+') as outfile:
        html = "<html><head></head><body>"
        html +='<h1 style="text-align:center"> Requête image ' + image + '</h1>' 
        html += '<div class="container">'
        for result in distances:
            img = result[0]
            #dist = result[1]
            html += '<a href="'+imagePath+img+'">'
            html += '<img src="'+imagePath+img+'" />'
            html += '</a>'
        html += "</div>"    
        html += "</body></html>"
        css = '<style>'
        css += '.container{display: flex; flex-wrap: wrap;} img{margin: 5px;transition: all 1s;}img:hover{transform: scale(1.1)}'
        css += '</style>'
        outfile.write(html)
        outfile.write(css)
        
def createIndexHTML(indexed):
    #Récupération des fichiers existants
    files = [] 
    for file in os.listdir(const.BASE10000_HTML_FOLDER):
        if file.endswith("html") and file != 'index.html':
            files.append(file)
            
    #Création de l'index
    with open(const.BASE10000_HTML_FOLDER+"index.html", 'w+') as outfile:
        html = "<html><head></head><body>"
        html +='<h1 style="text-align:center">Correspondance image </h1>' 
        html += '<div>'
        for i in indexed:
            value_index = -1
            description = i[0]
            imageName = i[1]
            htmlName = i[2]
            mode= i[3]
            try:
                value_index = files.index(htmlName)
            except:
                value_index = -1
            if(value_index > -1):
                html += '<li><a href="'+htmlName+'">'+ mode +" : " + description +' ('+imageName+')</a></li>'
        html += "</div>"    
        html += "</body></html>"
        outfile.write(html)

def createSimilariteHTML(requests,binsRGB,binsGREY,nbSearch=10):
    # Fichier qui sera indexé
    indexed = []
    
    # Requête RGB
    for rinfo in requests:
        for bins in binsRGB:
            description = rinfo[0] #Description de l'image
            image = rinfo[1] # nom du fichier de l'image 
            imageNameWithoutExt=image[:image.rfind('.')]
            jsonfile = const.BASE10000_DESCRIPTORS_FOLDER + const.HISTRGB_DESCRIPTORS_JSON.replace('#',str(bins))
            htmlName = imageNameWithoutExt+"_RGB_#x#x#".replace('#',str(bins))+".html"
            mode="RGB_#x#x#".replace('#',str(bins))
            # Création du  dictionnaire de distance
            dist_dict = QBE(jsonfile,image,nbSearch)
            if dist_dict:
                indexed.append((description,image,htmlName,mode))
                # Création des fichiers HTML
                createHTMLFromQBE(image,htmlName,dist_dict,const.BASE10000_IMAGES_FOLDER)
     # Requête grey
    for rinfo in requests:
        for bins in binsGREY:
            description = rinfo[0] #Description de l'image
            image = rinfo[1] # nom du fichier de l'image 
            imageNameWithoutExt=image[:image.rfind('.')]
            jsonfile = const.BASE10000_DESCRIPTORS_FOLDER + const.HISTGREY_DESCRIPTORS_JSON.replace('#',str(bins))
            htmlName = imageNameWithoutExt+"_GREY_#".replace('#',str(bins))+".html"
            mode="GREY_#".replace('#',str(bins))
            # Création du  dictionnaire de distance
            dist_dict = QBE(jsonfile,image,nbSearch)
            if dist_dict:
                indexed.append((description,image,htmlName,mode))
                # Création des fichiers HTML
                createHTMLFromQBE(description,image,htmlName,dist_dict,const.BASE10000_IMAGES_FOLDER)
    # Création de l'index
    createIndexHTML(indexed)