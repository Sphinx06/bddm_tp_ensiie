#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################
# Définition des constantes
###############################
BASE10000_FILES = "../Base10000_descriptors/Base10000_files.txt"
BASE10000_VT_FILES = "../Base10000/VT_files.txt"
BASE10000_VT_DESC = "../Base10000/VT_description.txt"

BASE10000_DESCRIPTORS_FOLDER="../Base10000_descriptors/"
BASE10000_HTML_FOLDER = "../Base10000_HTML/"
BASE10000_IMAGES_FOLDER = "../Base10000/images/"
BASE10000_PR_FOLDER = "../Base10000_PR/"

HISTRGB_DESCRIPTORS_JSON = "HistRGB_#x#x#.json"
HISTGREY_DESCRIPTORS_JSON = "HistGREY_#.json"

PR_RGB_CLASS_JSON = "PR_#_RGB.json"
PR_GREY_CLASS_JSON = "PR_#_GREY.json"
 
HISTGREY_MODE = "HistGREY"
HISTRGB_MODE = "HistRGB"