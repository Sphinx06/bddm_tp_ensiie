#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from matplotlib import pyplot as plt
import numpy as np
import json 
import os.path

import const
import similarite as sim

def getClassesFromVT(files,description):
    descVT = []
    fileVT = {}
    
    # Récupération des classes 
    with open(description,'r') as desc:
        for c in desc: 
            descVT.append(c.replace('\n',''))
            
    # Récupération des fichiers par classe 
    with open(files,'r') as fc:
        for i,classe in enumerate(fc): 
            desc = descVT[i]
            fileVT[desc] = classe.split()
            
    return fileVT

def getPrecisionRecallCurve(files,description,mode,classe="ALL"):
    # Création d'un dossier PR 
    if not os.path.exists(const.BASE10000_PR_FOLDER):
        os.makedirs(const.BASE10000_PR_FOLDER)
    
    # Récupération des classes VT 
    dict_class = getClassesFromVT(files,description)
    
    filenameMode = ""
    jsonfileBase = ""
    bins= []
    # MODE GREY
    if(mode=="GREY"):
        filenameMode = const.PR_GREY_CLASS_JSON
        jsonfileBase = const.BASE10000_DESCRIPTORS_FOLDER + const.HISTGREY_DESCRIPTORS_JSON
        logBins = "GREY_"
        # Classes utilisées
        bins=[16,64,256]
    elif(mode=="RGB"):
        filenameMode = const.PR_RGB_CLASS_JSON
        jsonfileBase = const.BASE10000_DESCRIPTORS_FOLDER + const.HISTRGB_DESCRIPTORS_JSON
        logBins = "RGB_"   
        # Classes utilisées
        bins=[2,4,6]

    # Dictionnaire de donnée
    allCPR = {}
    
    # Pour chaque classe d'images
    for c,filesVT in dict_class.items():
        # Vérification si classe à récupèrer
        if(classe == "ALL" or c == classe ):
            allCPR[c] = {}
            filename = filenameMode.replace("#",c) 
            # Pour chacun des bins
            for b in bins:
                print("##############")
                print(logBins + str(b) + " - " + str(c))
                print("##############")
                jsonfile = jsonfileBase.replace('#',str(b))
                dico = {}
                cPR = []
                # Récupération de tous les couple rappel/liste de précision asociée
                for nb,file in enumerate(filesVT):
                    print(str(c)+" fichier: " + str(nb))
                    dict_file = sim.QBE(jsonfile,file,-1) # Récupération du QBE
                    nbFound=0
                    for i,found in enumerate(dict_file):
                        if found[0] in filesVT:
                            nbFound += 1
                        p = nbFound / (i+1)
                        r = nbFound / len(filesVT)
                        dico.setdefault(r,[]).append(p)
                # Récupération de la moyenne de la précision fonction du rappel
                for r,p in dico.items():
                    pMean=np.mean(np.array(p))
                    cPR.append((r,pMean))
                allCPR[c][b] = cPR
            # Ecriture du fichier
            with open(const.BASE10000_PR_FOLDER+filename, 'w+') as outfile:
                json.dump(allCPR[c],outfile,indent=4)    

def createGlobalPRJSON(mode):
    #Récupération des fichiers existants
    filesGREY = [] 
    filesRGB = [] 
    result={}
    bins = []
    
    if (mode == "a"):
        bins= [16,64,256]
    elif (mode == "b"):
        bins = [2,4,6]
    elif (mode == "c"):
        bins = [256,6]
    
    for file in os.listdir(const.BASE10000_PR_FOLDER):
        if file.endswith("GREY.json"):
            filesGREY.append(file)
        if file.endswith("RGB.json"):
            filesRGB.append(file)
    
    for file in filesGREY:
        with open(const.BASE10000_PR_FOLDER + file,'r') as json_file:
             data = json.load(json_file)
             for (b,PR) in data.items():
                 # Bins autorisé en fonction du mode 
                 if int(b) in bins:
                     result.setdefault(b,[]).append(PR)
    
    for file in filesRGB: 
        with open(const.BASE10000_PR_FOLDER + file,'r') as json_file:
             data = json.load(json_file)
             for (b,PR) in data.items():
                 # Bins autorisé en fonction du mode 
                 if int(b) in bins:
                     result.setdefault(b,[]).append(PR)
    
    # Création des fichiers de résultats
    for (b,PR) in result.items():
        dico = {}
        nbCouple = {}
        for classList in PR:
            for couple in classList:
                r = couple[0]
                if r in dico:
                    dico[r] += couple[1]
                else:
                    dico[r] = couple[1]

                if r in nbCouple:
                    nbCouple[r] += 1
                else:
                    nbCouple[r] = 1
                    
        for r,p in dico.items(): 
            dico[r] = p/(nbCouple[r])
            
        result[b] =list(dico.items())

    return result
    
    

def plotPRC(classe,mode):
    # Vérification de l'existance de l'image
    filename = ""
    if(mode=="GREY"):
        filename = const.BASE10000_PR_FOLDER + const.PR_GREY_CLASS_JSON.replace("#",classe)
    elif(mode=="RGB"):
        filename = const.BASE10000_PR_FOLDER + const.PR_RGB_CLASS_JSON.replace("#",classe)
    
    if(os.path.exists(filename)):
        with open(filename) as json_data:
            dictPRC = json.load(json_data)
            for i,(b,pr) in enumerate(dictPRC.items()):
                r,p = zip(*pr)
                plt.plot(r,p,label=mode+"_"+str(b))
            plt.legend(loc='upper right')
            plt.ylabel('Précision')
            plt.xlabel('Rappel')
            plt.title("CLASSE - "+classe)
            plt.show()  