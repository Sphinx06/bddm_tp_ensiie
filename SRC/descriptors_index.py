#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from matplotlib import pyplot as plt
import cv2
import json 
import os.path

# Constantes const.py
import const

###############################
# Définition des fonctions
###############################
#---------------------------------------------------------
# Récupération d'un histogramme en niveau de gris
#-------------------------------------------------------
def ComputeGrayLevelHistogram(image,bins,plotting):
    # Vérification de l'existance de l'image
    if(os.path.exists(image)):
        #Récupération de l'image
        img = cv2.imread(image,cv2.IMREAD_GRAYSCALE)
        size = img.shape[0]*img.shape[1]
        # Calcul de l'histogramme
        hist = cv2.calcHist([img], [0], None, [bins], [0, 256])
        hist = hist.flatten()
        # Normalisation 
        hist = hist/size

        if plotting:
            plt.plot(hist)
            plt.title('Histogramme en niveau de gris, classe = '+str(bins))
            plt.show()
            
            plt.imshow(img,cmap='gray')
        return hist.tolist()
    else :
        return [];    

#---------------------------------------------------------
# Récupération d'un histogramme RGB
#---------------------------------------------------------
def ComputeRGBHistogram(image,lvlQT,plotting):
    # Vérification de l'existance de l'image
    if(os.path.exists(image)):
        # Récupération de l'image
        img = cv2.imread(image)
        # Récupération de la taille de l'image pour la future normalisation
        size = img.shape[0]*img.shape[1]
        # Calcul de l'histogramme
        hist = cv2.calcHist([img], [0, 1, 2],None, [lvlQT, lvlQT, lvlQT], [0, 256, 0, 256, 0, 256])
        # Récupération du vecteur x*x*x
        histFlatten = hist.flatten()
        # Normalisation 
        finalHist = histFlatten/size
        # Plot si demandé
        if plotting:
            plottingRGBHistogram(img,256)
        return finalHist.tolist()  
    else :
        return [];

#-------------------------------------------------------
# Affichage de l'histogramme 3D RGB applati 
#---------------------------------------------------------
def plottingRGBHistogram(image,bins):
    color = ('b','g','r')
    for i,col in enumerate(color):
        histr = cv2.calcHist([image],[i],None,[256],[0,256])
        plt.title("Histogramme RGB applati en 256 classes")
        plt.plot(histr,color = col)
        plt.xlim([0,256])
    plt.show()
    plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))


#---------------------------------------------------------
# Création d'une base d'index d'une bibiliothèque d'image        
#---------------------------------------------------------
def IndexDatabase(filename,imagesPath,mode,bins):
    
    # Déclaration des variables
    dname= "index";
    data = {} # Création de la bibliothèque d'index au format JSON 
                    
    # Vérification si fichier existant
    if os.path.exists(filename) :
        # Ouverture du fichier contenant la bibliothèque d'image
        file= open(filename,"r")
        data[dname]=[]
        print("Création de l'index pour le mode " + mode + " des images du fichier " + filename + ":")
        print("En cours...")
        
        # Pour chaque image de la bibliothèque
        for imgName in file:
            # Gestion du nom de l'image
            imgNameReformat = imgName.rstrip("\n")
            
            if(mode == const.HISTRGB_MODE):    
                # Récupération de l'histogramme 
                hists = ComputeRGBHistogram(imagesPath+imgNameReformat,bins,False)
                # Ajout des données dans le fichier de résultat JSON
                data[dname].append({"filename":imgNameReformat,"hist":hists})
            elif (mode == const.HISTGREY_MODE): 
                 # Récupération de l'histogramme 
                hist = ComputeGrayLevelHistogram(imagesPath+imgNameReformat,bins,False)
                # Ajout des données dans le fichier de résultat JSON
                data[dname].append({"filename":imgNameReformat,"hist":hist})
    print("Fin de la création de l'index.")
    return data 

#---------------------------------------------------------
# Création de l'index pour les images en niveau de gris
#---------------------------------------------------------
def IndexGreyLevelDatabase():
    # Création des index
    greyBins = [16,64,256] 
    for bins in greyBins:
        contents = IndexDatabase(const.BASE10000_FILES,const.BASE10000_IMAGES_FOLDER,const.HISTGREY_MODE,bins)
        filename = "./Base10000_descriptors/HistGREY_"+str(bins)+".json"
        # Ecriture du résulat dans un fichier JSON 
        with open(filename, 'w+') as outfile:
            json.dump(contents, outfile, sort_keys=True)
        print("Fichier " + filename + " crée.")

#---------------------------------------------------------
# Création de l'index pour les images RGB
#---------------------------------------------------------
def IndexRGBDatabase():
    
    # Création des index
    RGBBins = [2,4,6] 
    for bins in RGBBins:
        contents = IndexDatabase(const.BASE10000_FILES,const.BASE10000_IMAGES_FOLDER,const.HISTRGB_MODE,bins)
        filename = const.HISTRGB_DESCRIPTORS_JSON.replace("#",str(bins))
        
        # Ecriture du résulat dans un fichier JSON 
        with open(const.BASE10000_DESCRIPTORS_FOLDER+filename, 'w+') as outfile:
            json.dump(contents, outfile,sort_keys=True)
        print("Fichier " + filename + " crée.")