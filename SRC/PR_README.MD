
# getClassesFromVT(files,description):
- Description :
    - Récupère toutes les classes des fichier VT
    Ainsi que tous les fichiers pour chaque classe pour créer un dictionnaire
    en sortie.

- Paramètres:
    - files = Chemin du fichier VT_files.txt
    - description =  Chemin du fichier VT_description.txt
    
# getPrecisionRecallCurve(files,description,mode,classe="ALL"):
- Description :
    - Créer le fichier JSON de couple Pr pour les classes demandées, 
    dans le mode demandé.
    
- Paramètres:
    - files = Chemin du fichier VT_files.txt
    - description =  Chemin du fichier VT_description.txt
    - mode = GREY ou RGB
    - classe : Nom de la classe souhaité à produire contenu dans le fichier VT_description
                Par défaut: prends toutes les classes
# createGlobalPRJSON(mode):
- Description :
    - Récupère tous les fichiers JSON des couples PR d'un mode sélectionné.
    Créer une moyenne pour chaque bins demandé.
    Puis retourne un dictionnaire de données pour chacun de ces bins,
    contenant la moyenne de tous les couples PR pour toutes les classes.
    
- Paramètres:
    - mode = 
        Trois mode faisant référence aux exercices demandés:
            mode "a": bins [256,64,16] grey
            mode "b": bind [2,4,6] couleur 
            mode "c": bins [256,6] grey/couleur
    

# plotPRC(classe,mode):
- Description :
    Permet de d'afficher l'histogramme pour une classe et un mode sélectionné. 
    Les fichiers JSON de couple PR pour la classe demandé, doit être existant.
- Paramètres:
    - classe = Nom de classe demandé (doit être iso au nom de la classe dans le nom de fichier JSON)
    - mode = "RGB" ou "GREY" 