#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from matplotlib import pyplot as plt
import threading 
import multiprocessing

# FICHIERS PROJETS
import similarite as sim
import PR
import descriptors_index as desc 
import const

#---------------------------------------------------------
# MAIN 
#---------------------------------------------------------   
if __name__== "__main__":
       
    # Récupération choix 
    print("1 - Création des indexs en niveau de gris des images au format JSON.")
    print("2 - Création des indexs RGB des images au format JSON.")
    print("3 - Création des fichiers HTML de correspondances d'images.")
    print("4 - Affichage d'un exemple de diagrammes Précision Rappel (plot)")
    print("5 - Affichage des diagrammes Précision Rappel pour une classe au choix")
    print("6 - Affichage des diagrammes Précision Rappel globaux (toutes classes).")
    print("------------------------------------------------")   
    print("Les modes suivants sont désactivés dans le code, à décommenter pour tester.")
    print("7 - Création des fichiers PR JSON pour le mode GREY pour toutes les classes.")
    print("8 - Création des fichiers PR JSON pour le mode RGB pour toutes les classes.")
    ipt = input('Choix : ... ')
    
    # Création des index en niveau de gris
    if ipt == '1':
        print("Génération de l'index en niveau de gris.")
        print("1 à 2 minutes environ pour la génération")
        desc.IndexGreyLevelDatabase()      
        
    # Création de index RGB
    elif ipt == '2':
        print("Génération de l'index en RGB.")
        print("1 à 3 minutes environ pour la génération")
        desc.IndexRGBDatabase()
        
    # Création des fichiers HTML 
    elif ipt == '3':
        print("Combien d'images simalaire souhaitez-vous par requête ?")
        nbIpt = input('Nb : ... ')
        try:
            nbIpt = int(nbIpt)
        except:
            print("Problème lors de la saisie, valeur par défaut 10")
            nbIpt = 10
            
        # Variables
        requests = [('Carte de jeu','425002.jpg'),('Porte de paris bleue','549000.jpg'),('Fitness','282004.jpg'),('Lions','105077.jpg'),('Rhinos','112055.jpg'),('Chien','415039.jpg')]
        nbSearch = nbIpt # nombre d'images recherchés
        binsRGB = [2,4,6] # classes RGb
        binsGREY = [16,64,256] # classes GREY
        sim.createSimilariteHTML(requests,binsRGB,binsGREY,nbSearch)
                    
    # Exemple plot PR GREY classe CARDS
    elif ipt == '4':
        print("Lancement d'un exemple de courbe PR pour Fitness et Cards")
        PR.plotPRC("Fitness","GREY")
        PR.plotPRC("Fitness","RGB")
        PR.plotPRC("Cards","GREY")
        PR.plotPRC("Cards","RGB")
    
    # Sélection d'une classe à afficher 
    elif ipt == '5':
        print("IMPORTANT : Le fichier de la classe demandé doit être existant dans le dossier " + const.BASE10000_PR_FOLDER )
        print("Quel est le nom de la classe que vous souhaitez-voir en diagramme PR ?")
        classe = input('Classe: ')
        try:
            PR.plotPRC(classe,"GREY")
            PR.plotPRC(classe,"RGB")
        except: 
           print("Un problème est apparu lors de la création des diagrammes.")
 
        
    # Affichage des diagrammes de l'exercice 4
    elif ipt == '6': 
        print("Quel mode voulez-vous lancer  ?")
        print("1 = MODE A -Toutes les classes GREY.")
        print("2 = MODE B -Toutes les classes RGB.")
        print("3 = MODE C -RGB 6x6x6 et GREY 256.")
        
        modeIpt = input('... ')
        
        try:
            title = ""
            label = ""
            mode = "a"
            if(1 == int(modeIpt)):
                title = "Toutes les classes - GREY"
                label = "GREY_"
                mode = "a"
            elif(2 == int(modeIpt)):
                title = "Toutes les classes - RGB"
                label = "RGB_"
                mode = "b"
            elif(3 == int(modeIpt)):
                title = "RGB 6x6x6 et GREY 256 "
                label = ""
                mode = "c"
            else: 
                print("Problème lors de la saisie, valeur par défaut mode A")
                
            test = PR.createGlobalPRJSON(mode)
            for i,(b,pr) in enumerate(test.items()):
                (r,p) = zip(*pr)
                plt.plot(r,p,label=label+str(b))
            plt.legend(loc='upper right')
            plt.ylabel('Précision')
            plt.xlabel('Rappel')
            plt.title(title)
            plt.show()  
        except: 
             print("Mode sélectionné inconnue.")
    
    ###################################################################
    # DESACTIVER POUR EVITER DE LANCER LE LONG PROCESSUS PAR ERREUR
    # EN MOYENNE UNE HEURE PAR MODE DE CREATION 
    ###################################################################
    
    # Création des fichiers PR JSON POUR LE MODE RGB
# =============================================================================
#     elif ipt == '7':
#         nbThread = multiprocessing.cpu_count()
#         threads = []
#         test=PR.getClassesFromVT(const.BASE10000_VT_FILES,const.BASE10000_VT_DESC)
#         cl = list(test.keys())        
# 
#         print("Lancement de la génération des fichiers par thread de "+str(nbThread) + " .")
#         for i in range(0,len(cl),nbThread):
#             end=i+nbThread
#             if (end > len(cl)):
#                 end = len(cl)
#             
#             for c in cl[i:end]:
#                 print("Lancement classe:" + c)
#                 x = threading.Thread(target=PR.getPrecisionRecallCurve, args=(const.BASE10000_VT_FILES,const.BASE10000_VT_DESC,"GREY",c,))
#                 threads.append(x)
#                 x.start()
#             for index, thread in enumerate(threads):
#                 thread.join()
#                 
#     # Création des fichiers PR JSON POUR LE MODE RGB
#     elif ipt == '8':
#         nbThread = 30
#         threads = []
#         test=PR.getClassesFromVT(const.BASE10000_VT_FILES,const.BASE10000_VT_DESC)
#         cl = list(test.keys())        
# 
#         print("Lancement de la génération des fichiers par thread de "+str(nbThread) + " .")
#         for i in range(0,len(cl),nbThread):
#             end=i+nbThread
#             if (end > len(cl)):
#                 end = len(cl)
#             
#             for c in cl[i:end]:
#                 print("Lancement classe:" + c)
#                 x = threading.Thread(target=PR.getPrecisionRecallCurve, args=(const.BASE10000_VT_FILES,const.BASE10000_VT_DESC,"RGB",c,))
#                 threads.append(x)
#                 x.start()
#             for index, thread in enumerate(threads):
#                 thread.join()
# =============================================================================
            
        
